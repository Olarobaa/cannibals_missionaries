package cannibals_missionaries;

import java.util.Vector;

public class Cannibals_missionaries {
    
   static class Coast {
    int cannibals;
    int missionaries;
    boolean boat;
    int cannibals2;
    int missionaries2;
    
    public  Coast(int c, int m) {
         cannibals = c; //kannibálok bal parton
         missionaries = m; //misszionáriusok bal parton
         boat = true; //hajó bal oldalról indul, jobb oldal = false
         cannibals2 = 0; //kannibálok jobb parton
         missionaries2 = 0; //misszionáriusok jobb parton
        }
    
    public Coast(Coast new_coast) {
        cannibals = new_coast.cannibals;
        missionaries = new_coast.missionaries;
        boat = new_coast.boat;
        cannibals2 = new_coast.cannibals2;
        missionaries2 = new_coast.missionaries2;
    }
    
    };
    
    public static Coast cannibals(Coast coast) { //két kannibált viszünk egyik oldalról a másikra
        if(coast.boat) {
            coast.cannibals -= 2;
            coast.boat = false;
            coast.cannibals2 += 2;
        } else {
            coast.cannibals += 2;
            coast.boat = true;
            coast.cannibals2 -= 2;
        }
        return coast;
   };

    public static Coast missionaries(Coast coast) { //két misszionáriust viszünk egyik oldalról a másikra
        if(coast.boat) {
            coast.missionaries -= 2;
            coast.boat = false;
            coast.missionaries2 += 2;
        } else {
            coast.missionaries += 2;
            coast.boat = true;
            coast.missionaries2 -= 2;
        }
        return coast;
   };

    public static Coast cannibalAndMissionary(Coast coast) { //egy kannibált és egy misszionáriust
        if(coast.boat) {                                     //viszünk egyik oldalról a másikra
            --coast.cannibals;
            --coast.missionaries;
            coast.boat = false;
            ++coast.cannibals2;
            ++coast.missionaries2;
        } else {
            ++coast.cannibals;
            ++coast.missionaries;
            coast.boat = true;
            --coast.cannibals2;
            --coast.missionaries2;
        }   
        return coast;
   };

    public static Coast oneCannibal(Coast coast) { //egy kannibált viszünk egyik oldalról a másikra
        if(coast.boat) {
            --coast.cannibals;
            coast.boat = false;
            ++coast.cannibals2;
        } else {
            ++coast.cannibals;
            coast.boat = true;
            --coast.cannibals2;
        }
        return coast;
   };

    public static Coast oneMissionary(Coast coast) { //egy misszionáriust viszünk egyik oldalról a másikra
        if(coast.boat) {
            --coast.missionaries;
            coast.boat = false;
            ++coast.missionaries2;
        } else {
            ++coast.missionaries;
            coast.boat = true;
            --coast.missionaries2;
        }
        return coast;
   };
    
 
    public static boolean mealCheck(Coast coast) { //kannibál többség
         if(((coast.cannibals > coast.missionaries) && coast.missionaries > 0) || ((coast.cannibals2 > coast.missionaries2) && coast.missionaries2 > 0)){
            return true;
        };
        return false;
    }
    
    public static boolean isSolved(Coast coast) { //megoldás
        return coast.cannibals == 0 && coast.missionaries == 0 && coast.boat == false;
    }
    

      public static boolean isVisited(Vector<Coast> states, Coast new_coast){ //már bejárt állapot-e
          for(int i = 0; i < states.size(); ++i) {
            if (          
                    states.elementAt(i).boat == new_coast.boat
                   && states.elementAt(i).cannibals == new_coast.cannibals
                    && states.elementAt(i).missionaries == new_coast.missionaries
                    && states.elementAt(i).cannibals2 == new_coast.cannibals2
                    && states.elementAt(i).missionaries2 == new_coast.missionaries2){
                return true;
            }
        }
        return false;
    }
    
    
    public static void copyCoast(Coast coast, Coast coast2) { //értékek másolása partok közt
        coast.cannibals = coast2.cannibals;
        coast.missionaries = coast2.missionaries;
        coast.boat = coast2.boat;
        coast.cannibals2 = coast2.cannibals2;
        coast.missionaries2 = coast2.missionaries2;
    }
    
    
    public static void print(Coast coast) { //az állapot kiíratása
        System.out.println("Left cannibals: " + coast.cannibals + "  Left missionaries: "+  coast.missionaries + "     " + coast.boat + "    "
        + "  Right cannibals:  " + coast.cannibals2 + "  Right missionaries: " + coast.missionaries2);
    }
    
    public static void solve () { //megoldó függvény
        Coast coast = new Coast(3,3); //a kezdeti part, amelynek ha az állapota 0 0 false 3 3, akkor van megoldás
        Coast new_coast =  new Coast(3,3); //teszt part, amely ellenőrzi, hogy lehetséges-e a következő állapot
        boolean solved = false; //megoldás állapota
        int same_step = 5; //ugyanazokat a lépéseket nézi, hogy ne ismétlődjenek egymás után
        Vector<Coast> states = new Vector<Coast>(); //az eddigi bejárt állapotok tömbje
        states.add(new Coast(coast));
        print(coast);
        do{
           
            loop: for(int i = 0; i<5; ++i){
               if(i != same_step)             
                 
                 switch(i){

                     case 0:
                       if((coast.cannibals >= 2 && coast.boat) || (coast.cannibals2 >= 2 && !coast.boat)) {
                             cannibals(new_coast);
                         } else {
                             continue; }
                        if( isVisited(states,new_coast) || mealCheck(new_coast)){
                            copyCoast(new_coast,coast);
                        } else {
                            copyCoast(coast,new_coast);
                            same_step = i;
                            states.add(new Coast(coast));
                            print(coast);
                            break loop;
                        }
                         break;
                     
                     case 1:
                        if((coast.missionaries >= 2 && coast.boat) || (coast.missionaries2 >= 2 && !coast.boat)) {
                             missionaries(new_coast);
                         } else {
                             continue; }
                        if( isVisited(states,new_coast) || mealCheck(new_coast)){
                            copyCoast(new_coast,coast);
                        } else {
                            copyCoast(coast,new_coast);
                            same_step = i;
                            states.add(new Coast(coast));
                            print(coast);
                            break loop;
                        }
                         break;
                     
                     case 2:
                      
                        if((coast.cannibals >= 1 && coast.missionaries >= 1 && coast.boat) || (coast.cannibals2 >= 1 && coast.missionaries2 >= 1 && !coast.boat)) {
                             cannibalAndMissionary(new_coast);
                         } else {
                             continue;}
                        if( isVisited(states,new_coast) || mealCheck(new_coast)){
                            copyCoast(new_coast,coast);
                        } else {
                            copyCoast(coast,new_coast);
                            same_step = i;
                            states.add(new Coast(coast));
                            print(coast);
                            break loop;
                        }
                         break;
                     
                     case 3:
                        if((coast.cannibals >= 1 && coast.boat) || (coast.cannibals2 >= 1 && !coast.boat)) {
                             oneCannibal(new_coast);
                         } else {
                             continue; }
                        if( isVisited(states,new_coast) || mealCheck(new_coast)){
                            copyCoast(new_coast,coast);
                        } else {
                            copyCoast(coast,new_coast);
                            same_step = i;
                            states.add(new Coast(coast));
                            print(coast);
                            break loop;
                        }
                         break;
                     
                     case 4:
                      
                         if((coast.missionaries >= 1 && coast.boat) || (coast.missionaries2 >= 1 && !coast.boat)) {
                             oneMissionary(new_coast);
                         } else {
                             continue; }
                        if( isVisited(states,new_coast) || mealCheck(new_coast)){
                            copyCoast(new_coast,coast);
                        } else {
                            copyCoast(coast,new_coast);
                            same_step = i;
                            states.add(new Coast(coast));
                            print(coast);
                            break loop;
                        }
                         break;
                 }
             }
            
            if(isSolved(coast)) solved = true;
        }while(!solved);
    }
    
    public static void main(String[] args) {
        solve();
    }
}
